<?php

  function welcome($adminpassword,$skinpath,$versioninfo, $dbhost, $dbuser, $dbpass, $dbname, $htmlpath, $iev, $ma, $mb,$g1,$g2)//welcomes the user
  {
/*Performs basic greeting and decides the primary mode that Earnest starts in
Also checks for database functionality and proper installation */
    include("$skinpath/welcome.html");
      
      //test database connection
            if (!test_database($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname)) {
         include("$skinpath/dberror.html");
          exit;
      } else {
          include("$skinpath/dbok.html");
      }
      $numsolutions = count_solutions($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname);
      if ($numsolutions == "0") {
          include($g1);
          $ret[0]=$ma;
	  $ret[1]=0;
	  return($ret);
      } else {
         include($g2);

		$ret[0]=$mb;
		$ret[1]=1;    
 return($ret);
      }
  }
  
  
  
  //********input functions the $m parmeter represents the return mode that will handle the forms posting
  //more info in function_prototypes.txt
  
  
  function input_problem($adminpassword,$skinpath,$resource, $m,$iev,$versioninfo)//present an input box to ask a problem
  {
      include($resource);
  }
  
  function search_problem($adminpassword,$skinpath,$resource, $m,$iev,$versioninfo)//present an input box to search for a problem
  {
      include($resource);
  }
  
  function input_solution($adminpassword,$skinpath,$problem_id, $question_tags, $question_tags_old, $resource, $m,$iev,$versioninfo,$problemx)//present an input box to ask a question
  {
      global $question_tags;
      include($resource);
  }
  
  function input_question($adminpassword,$skinpath,$problem_id, $question_tags, $problem, $resource, $m,$iev,$versioninfo)//present an input box to ask a question
  {
      $problemx = urlencode($problem);
      include($resource);
  }
  
  function input_question2($adminpassword,$skinpath,$problem_id, $question_tags, $problem, $question_tags_old, $resource, $m,$iev,$versioninfo)//present an input box to ask a question
  {
      $problemx = urlencode($problem);
      include($resource);
  }
  
  

  /***********  OUTPUT FUNCTIONS ******************************/

  
  function save_question($adminpassword,$skinpath,$question, $dbhost, $dbuser, $dbpass, $dbname, $problem_id, $update,$iev,$versioninfo)//add question text to the question database database
  {
      global $question_tags;
      // Connect to MySQL Database
      
      mysql_connect($dbhost, $dbuser, $dbpass);
      
      mysql_select_db($dbname) or die("Unable to select database");
      $sql = "INSERT INTO `questions` (`question_id`, `question_text`) VALUES (NULL, '" . mysql_real_escape_string($question) . "');";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
      $question_id = mysql_insert_id();
      //!!!! need to save newly generated question id in the existing problem_tag list by concatenation
      $problem_id = sprintf("%010d", $problem_id);
      //get problem_tag for problem_id and then add ,question_id on end then resave to table
      $sql = "SELECT * FROM `problems` WHERE `problem_id` = $problem_id";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
      while ($row = mysql_fetch_assoc($result)) {
          $problem_tag = $row['problem_tags'];
      }
      //add the question_id to the problem_tags and update problems table
      $problem_tags = sprintf("%s%s,", $problem_tag, $question_id);
      $question_tags = $problem_tags;
      $sql = "UPDATE `problems` SET `problem_tags` = '$problem_tags' WHERE `problem_id` = $problem_id ";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
      mysql_close();
      return($problem_id);
  }
  
  
  
  function save_question2($adminpassword,$skinpath,$question, $dbhost, $dbuser, $dbpass, $dbname, $problem_id, $update, $question_tags, $question_tags_old,$iev,$versioninfo)//add question text to the question database database
  {
      global $question_tags;
      global $question_tags_old;
      // Connect to MySQL Database      
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");
      $sql = "INSERT INTO `questions` (`question_id`, `question_text`) VALUES (NULL, '" . mysql_real_escape_string($question) . "');";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
      $question_id = mysql_insert_id();
      $question_tags_old = sprintf("%s%s,", $question_tags_old, $question_id);
      //!!!! need to save newly generated question id in the existing problem_tag list by concatenation
      $problem_id = sprintf("%010d", $problem_id);
      //get problem_tag for problem_id and then add ,question_id on end then resave to table
      $sql = "SELECT * FROM `problems` WHERE `problem_id` = $problem_id";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
      while ($row = mysql_fetch_assoc($result)) {
          $problem_tag = $row['problem_tags'];
      }
      //add the question_id to the problem_tags and update problems table
      $problem_tags = sprintf("%s%s,", $problem_tag, $question_id);
      $sql = "UPDATE `problems` SET `problem_tags` = '$problem_tags' WHERE `problem_id` = $problem_id ";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
      mysql_close();
      return($problem_id);
  }
  
  
  
  
  
  
  function save_problem($adminpassword,$skinpath,$problem, $dbhost, $dbuser, $dbpass, $dbname,$iev,$versioninfo)//add question text to the question database database
  {
      // Connect to MySQL Database
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");
      $sql = "INSERT INTO `problems` (`problem_id`, `problem_description`) VALUES (NULL, '" . mysql_real_escape_string($problem) . "');";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
      $problem_id = mysql_insert_id();
      mysql_close();
      //send the problem id back to the main routine so it can tag questions to the problem
      return($problem_id);
  }
  
  
  
  function save_solution($adminpassword,$skinpath,$solution, $dbhost, $dbuser, $dbpass, $dbname, $question_tags2, $question_tags_old2, $password,$iev,$versioninfo)//add question text to the question database database
  {
      $match=0;
      global $question_tags;
      if (strlen(trim($question_tags_old2)) != 0) {
          $question_tags2 = $question_tags_old2;
      } else {
      }
      // Connect to MySQL Database      
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");
      $sql = "SELECT * FROM `solutions` WHERE `solution_text` LIKE '$solution' LIMIT 1";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
              while ($row = mysql_fetch_assoc($result)) {
		$solution_id=$row['solution_id'];
		$match=1;                  
              }
	if($match==0) //no solution matches this so add it
	{
	      $sql = "INSERT INTO `solutions` (`solution_id`, `solution_text`, `solution_tags`, `solution_password`) VALUES (NULL, '" . mysql_real_escape_string($solution) . "' , '" . mysql_real_escape_string($question_tags2) . "','" . mysql_real_escape_string($password) . "');";
	      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
	      $psolution_id = mysql_insert_id();
	      mysql_close();
	      //send the problem id back to the main routine so it can tag questions to the problem
	      return($solution_id);
	}
	else  //there is a solution that is the same so update it
	{
	$sql = "UPDATE `solutions` SET `solution_tags` = '" . mysql_real_escape_string($question_tags2) . "' WHERE `solution_id` = $solution_id ";
	 $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
	   mysql_close();    
	}
  }
  

  //******************************************************************//


 
  
  
  function list_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $yesno,$iev)
  {
      // Connect to MySQL Database
      include("$skinpath/welcome.html");      
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");
      //get problem_tags and create an array of question_ids to ask the user
      $question_ids = split(",", $question_tags);      
      if (strlen(trim($question_ids[0])) != 0) {
          for ($x = 0; $x < count($question_ids); $x++) {
              $question_id = sprintf("%010d", $question_ids[$x]);
              $sql = "SELECT * FROM `questions` WHERE `question_id` = '$question_id'";
              $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
              while ($row = mysql_fetch_assoc($result)) {
                  $y = $x + 1;
                  $question_text = $row['question_text'];
                  echo "<b>Earnest:> </b>$y $question_text - [$yesno]<br>"; //needs exporting
              }
          }
      } 
      mysql_close();
  }
  
  
  
  function count_solutions($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname)
  {
      // Connect to MySQL Database      
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");
      $sql = "SELECT * FROM `solutions` ";      
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());      
      while ($row = mysql_fetch_assoc($result)) {
          $solutions++;
      }
      mysql_close();
      if ($solutions == 0) {
          $solutions = "0";
      }
      return($solutions);
  }
  

function aci($dbhost, $dbuser, $dbpass, $dbname, $question_id,$problem_tags,$question_tags,$problem_id)
{


$o="";
//working now butneed to modify it so that when we get to the last 2 descisions we then ask all remaining quetions 
//or else we get cut short, could possibly change the mode to 0 and continue with standard search at this stage

$question_id=sprintf("%d",$question_id); //turn into an int from 0000047 format
 // Connect to MySQL Database      
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");


 $sql = "SELECT * FROM `problems` WHERE `problem_id` LIKE '%%$problem_id%%' LIMIT 1";      
$result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());  
while ($row = mysql_fetch_assoc($result)) 
	{

$original_problem_tags=$row['problem_tags'];
}

      $sql = "SELECT * FROM `solutions` WHERE `solution_tags` LIKE '%%$question_tags%%' ";      
//echo "<font color=#FFFFFF>$sql</font><br>";
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());  
  

      while ($row = mysql_fetch_assoc($result)) 
	{
		$solution_tags=$row['solution_tags']; // get the tags for this solution
		$solution_id=$row['solution_id'];
		$solution_text=$row['solution_text'];
		//echo "<font color=#FFFFFF face='arial'> ID $solution_id: $solution_text: soltags $solution_tags </font><br>";
		//echo "<small><font color=#00FF00 face='arial'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Possibly: $solution_text<br></font></small>";
				
		$l++;
		$tags=split(",",$solution_tags); //split into an array
		//stripoff the a if its a no just so we can value it
		
		for($a=0;$a<count($tags)-1;$a++)
		{
		$f++;
		$tmp=sprintf("%d",str_replace("a","",$tags[$a]));
		$tmp2=sprintf("%d",str_replace("a","",$question_id));  //echo "$tmp - $tmp2<br>";
		//echo "<font color=#FFFFFF> $tags[$a] -- $question_tags<br>";
		if(trim($question_id) != trim($tags[$a]) & stristr($question_tags,trim($tags[$a]))===FALSE & stristr($tags[$a],"a")===FALSE & $tmp >= $tmp2  &$tmp2!=0)
		$problem_tags=sprintf("%s%s,",$problem_tags,$tags[$a]);
//echo "<font color=#FFFFFF> $problem_tags<br>";
		}
     	}
      mysql_close(); 
if($l==2)
{
$h=0;
$o=$original_problem_tags;
}
else
{
$h=2;
}

if($f!=0)
{
$x=$problem_tags;  
$x=split(",",$x);

sort($x);$x=array_unique($x);
$z=array_shift($x);
$y=implode(",",$x); 
$y=sprintf("%s,",$y);

$x="";
$x[0]=$h;
$x[1]=$y;
$x[2]=$o;

      return($x);
}
else
{
$x="";
$x[0]=$h;
$x[1]="";
$x[2]=$o;
return($x);

}
}
  
  
  
  function search_problems($adminpassword,$skinpath,$term, $dbhost, $dbuser, $dbpass, $dbname, $m, $h,$m2,$iev,$versioninfo)
  {
      include("$skinpath/welcome.html");
      $test=0;
      $term = strip_tags($term);
      $term=preg_replace('/\?/', '', $term, 1); //get rid of trailing ?
      $term=trim($term); //get rid of trailing white space
      // Connect to MySQL Database
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");
	//is this a question that requires a yes no answer?
	//eg does it start with: can, will, does, do, have,is?
	$preps = "will,has,does,do,can,is,have";
	$ytemp=split(" ",$term);
	if(@stristr($preps,trim($ytemp[0]))!==FALSE)
	{
	$yesno="Yes, ";
	}
      	//ignore all of the terms in this list
      	$ignore = "an,if,when,for,and,how,many,what,do,you,is,can,should,go,a,in,my,the,does,have,where,of,will,tell,me";
     	$ignore_list = split(",", $ignore);     

      for ($n = 0; $n < count($ignore_list); $n++) {
          $term = str_replace_word($ignore_list[$n], "###", $term);
      }
      
      if(!check_profanity($term))
	  {
	     include("$skinpath/not_allowed.html");
	    exit();
	  }      
      
	if($test==1)echo "Term $term. Pass 1<br>";
  
      $term = str_replace("###", "", $term);
      $term = str_replace(",", "", $term);
      $term = str_replace(" ", ",", $term);
      $term = str_replace(",,", ",", $term);
      $term = str_replace(",,", ",", $term);
      $term = str_replace(",,", ",", $term);

      for ($n = 0; $n < count($ignore_list); $n++) {
          $term = str_replace_word($ignore_list[$n], "###", $term);
      }      

        if(!check_profanity($term))
	  {
	     include("$skinpath/not_allowed.html");
	    exit();
	  }

	if($test==1)echo "Term $term. Pass 2<br>";    

	 $term = str_replace("###", ",", $term);    
	 $term = str_replace(",,,", ",", $term);
	 $term_list = split(",", $term);

	if($test==1)echo "Term $term. Pass 3<br>";  

	      //if term 0 is empty then rotate the array left 1
		   
	if(strlen(trim($term))!=0)
	{
	      if (trim($term_list[0]) == "") {
		 array_shift($term_list);
		if($test==1)echo "stripping leading comma<br>";
	      }

	     $t=count($term_list)-1;
	     if (trim($term_list[$t]) == "" & $t>1) {
		array_pop($term_list);
		if($test==1)echo "stripping trailing term<br>";
	      }
	}
        $x = 0;


switch($h)
{
case 0:
include("$skinpath/search_problem_result_header.html");
break;


case 1:
compute_solution($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id,$m,$h,$m2,$term_list,$yesno,$iev,$versioninfo);
exit;
break;

case 2:
include("$skinpath/search_problem_result_header.html");
break;

}



for ($n = 0; $n < count($term_list); $n++) {
          //now search problems table for problems that contain terms selected
     //echo "Term $term_list[$n]<br>";
     
          $sql = "SELECT * FROM `problems` WHERE `problem_description` LIKE '%$term_list[$n]%' AND `problem_description` NOT LIKE '%#%'";
          //echo "$sql<br>";
          
          $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());

         // echo "<ul>";
          while ($row = mysql_fetch_assoc($result)) {
              $problem_id = $row['problem_id'];
              
              $problem_descripton = $row['problem_description'];
              $problem_tags = $row['problem_tags'];
              if (stristr($problems_found, $problem_id) == false) {
                  $x++;
                  $problem_descripton2 = urlencode($problem_descripton);

//This is the main question table output routine, its one of the few hardcoded layout options in Earnest.
//you can edit this to your needs.
                  echo "$x) <a href=index.php?mode=$m&problem_id=$problem_id&h=$h&action=1&problem_description=$problem_descripton2&problem_tags=$problem_tags  method=POST>$problem_descripton</a> <br>";
//end editable section
                  $problems_found = sprintf("%s,%s", $problems_found, $problem_id);
              }
          }
       }
      //end for
      if ($x != 0) {
include("$skinpath/search_problem_result_footer.html");
      } else {

         include("$skinpath/no_result.html");

      }
      
      return("non");
  }
  
  




  function ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, $m,$iev,$versioninfo,$h,$aci)
  {
      global $questions_asked;
      // Connect to MySQL Database
      
      mysql_connect($dbhost, $dbuser, $dbpass);
      
      mysql_select_db($dbname) or die("Unable to select database");
      //get problem_tags and create an array of question_ids to ask the user
      $question_ids = split(",", $problem_tags);
      $question_id = $question_ids[0];
      $question_id = sprintf("%010d", $question_id);
      //dismiss alread asked questions and get the next in the list by removing the problem_tag matching this question before retutning to the mai routine.
      $problem_tags = str_replace("$question_ids[0],", "", $problem_tags);
      
      if (strlen(trim($question_ids[0])) != 0) {
          $sql = "SELECT * FROM `questions` WHERE `question_id` = '$question_id'";
          $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
          while ($row = mysql_fetch_assoc($result)) {
              $question_text = $row['question_text'];
          }
          
          $problem_description = urlencode($problem_description);
          $qt = stripslashes($question_text);
	 $problem_id=strip_tags($problem_id);
	 $problemx=strip_tags($problemx);
	 $m=strip_tags($m);
	 $question_tags=strip_tags($question_tags);
	 $question_tags_old=strip_tags($question_tags_old);
	 $term=strip_tags($term);
	 include("$skinpath/ask_question.html"); //create a form that passes back all the needed info to ask the next question
		  
	      }
	      
	      else {
			  $problem_description = urlencode($problem_description);
			  $problem_id=strip_tags($problem_id);
			  $problemx=strip_tags($problemx);
			  $m=strip_tags($m);
			  $question_tags=strip_tags($question_tags);
			  $question_tags_old=strip_tags($question_tags_old);
			  $term=strip_tags($term);
			  include("$skinpath/conclusion_form.html");
		   }
  }
  
  
  
  //***************************  MISC FUNCTIONS + UTILSform ******************//

  function output_xml($ret)
  {

$xml="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>
<!-- Edited by EarnestAI -->
<output>	
	<result>$ret</result>
</output>
";

	$fp=fopen("output.xml","w");
	fwrite($fp,$xml,strlen($xml));
	fclose($fp);
	

  }

  
  function test_database($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname)
  {
      // Connect to MySQL Database      
      mysql_connect($dbhost, $dbuser, $dbpass);      
      mysql_select_db($dbname) or die("Unable to select database");
      mysql_close();
      return(true);
  }
  
  
  function str_replace_word($needle, $replacement, $haystack)
  {
      $pattern = "/\b$needle\b/i";
      $haystack = preg_replace($pattern, $replacement, $haystack);
      return $haystack;
  }
  
  
  function check_auth($skinpath,$adminpassword,$check,$versioninfo,$iev)
  {
  //stop anyone teaching him stuff who is not authorized
           if (stristr($check, $adminpassword)===FALSE)
	{
	      include("$skinpath/not_auth.html");
		exit();
        }
	else
	{
	$check=str_replace($adminpassword,"",$check);
	return($check);
	}
  }

//reject profanity and banned words
  
    function check_profanity($check)
	{
 	    $profanity = "fuck,fuckoff,fuk,pissoff,wank,shit,cunt,prick,dick,twat,piss,twat,dildo,shag,peado,clit";
	    $profanity_list = split(",", $profanity);
	      for ($n = 0; $n < count($profanity_list); $n++) {
		  if (stristr($check, $profanity_list[$n])) {
		      return(FALSE);
		  }
	      }

		return(TRUE);
	}
  
?>
