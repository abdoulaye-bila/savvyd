<?php

  include("log.php");  //los access to the program in log.txt
  include("config.php");   //config script
  include("lib_basic.php");  //main functions of the EarnestAI program
  include("lib_inference.php");  //Main AI inference engine for EarnestAI


  //global variables
  $questions_asked = "";
  $versioninfo="1.000.13 PRE-ALPHA";
  
  //welcome the user if first run and give some basic instruction pass optional html pathname
  if (!$action){
     $ret = welcome($adminpassword,$skinpath,$versioninfo, $dbhost, $dbuser, $dbpass, $dbname, "$skinpath/welcome.html", $iev, 0, 1,"$skinpath/greeting_1.html","$skinpath/greeting_2.html");
	$mode=$ret[0];}
  
  //decide what to do based on the mode returned by each function
  // functions are passed a mode $m parameter which is POSTED back to the index.php specifying what code block to execute next
  switch ($mode) {

      case 0: 
	  //prompt the user to input a problem         
          input_problem($adminpassword,$skinpath,"$skinpath/problem_form.html", "3",$iev,$versioninfo);
          break;
          
      case 1:  
	  //prompt the user to search for a problem or input a new problem to learn         
          search_problem($adminpassword,$skinpath,"$skinpath/search_problem.html", "5",$iev,$versioninfo);
          input_problem($adminpassword,$skinpath,"$skinpath/problem_form.html", "3",$iev,$versioninfo);
          break;

      case 2:
          //the question has been asked, now add it to the questions database
          $problemx = urldecode($problem);
	  include("$skinpath/welcome.html");
	  $question=check_auth($skinpath,$adminpassword,$question,$versioninfo,$iev);
          save_question($adminpassword,$skinpath,$question, $dbhost, $dbuser, $dbpass, $dbname, $problem_id, $updateonly,$iev,$versioninfo);        
	  include("$skinpath/input_question_header_2.html");
          input_question($adminpassword,$skinpath,$problem_id, $question_tags, $problemx, "$skinpath/input_question.html", "2",$iev,$versioninfo);
          include("$skinpath/request_solution.html");
          input_solution($adminpassword,$skinpath,$problem_id, $question_tags, "", "$skinpath/input_solution.html", "4",$iev,$versioninfo,$problemx);
          break;
          
      case 3:
          //the problem has been asked, now add it to the questions database
	  //reject profianity!!!

	  if(!check_profanity($problem))
	  {
	     include("$skinpath/welcome.html");
	     include("$skinpath/not_allowed.html");
	    exit();
	  }
	  include("$skinpath/welcome.html");     
          $problem=check_auth($skinpath,$adminpassword,$problem,$versioninfo,$iev);

          $p = save_problem($adminpassword,$skinpath,$problem, $dbhost, $dbuser, $dbpass, $dbname,$iev,$versioninfo);
	       
	  include("$skinpath/input_question_header_1.html");
          input_question($adminpassword,$skinpath,$p, $question_tags, urlencode($problem), "$skinpath/input_question.html", "2",$iev,$versioninfo);
          break;
          
      case 4:
          //save the solution and the answers that lead to it.
	  include("$skinpath/welcome.html");
          $solution=check_auth($skinpath,$adminpassword,$solution,$versioninfo,$iev);
          $s = save_solution($adminpassword,$skinpath,$solution, $dbhost, $dbuser, $dbpass, $dbname, $question_tags2, $question_tags_old2, $password,$iev,$versioninfo);
          
          include("$skinpath/thank_you.html");
          break;
          
      case 5:
          //search for a problem to which i have a known answer

if($h=="3")
{
echo "GIS";
 $ret=compute_solution($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id,"8",$h,0,NULL,NULL,$iev,$versioninfo);
//echo"<script javascript>alert('$ret');</script>";
output_xml($ret);
exit; 
}                    




          $prob_list = search_problems($adminpassword,$skinpath,$term, $dbhost, $dbuser, $dbpass, $dbname, "6",$h,"8",$iev,$versioninfo);
          break;
          
      case 6:
	  include("$skinpath/welcome.html");
          $problem_description = urldecode($problem_description);
          //start asking YES NO questions based on the problems_tags required to fulfill the solution
          //ask each question in the list and prompt for a yes no dont know answer, pass a list of already asked question so we can keep track
          
	
	if ($action != 4) //ask another question
	{
		ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, "6",$iev,$versioninfo,$h,0);
	}

          
          
        if ($action == 4) {
if($h==2){$problem_tags="";}

	switch($h)
	{

	case "0": //basic forward chaining	
//$ret=compute_solution($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id,"8",$h,0,NULL,NULL,$iev,$versioninfo);

//echo "$ret <br>";

//if this is the first calling from previously ACI call then just update the problem tags one then leave alone
if($aci==1)
{
$p=aci($dbhost, $dbuser, $dbpass, $dbname, $question_id,$problem_tags,$question_tags,$problem_id);
$original_problem_tags=$p[2];
$problem_tags=$p[1];
//now break the original problem tags up and remove all questions before "last_question"
$qtt=split($question_id,$original_problem_tags);
$problem_tags=$qtt[1];
$problem_tags = ltrim($problem_tags, ","); 
}

//echo "<font color=\"#FFFFFF\">Original problem tags :$original_problem_tags<br></font>";

              switch ($submit) {
                  case "No":
                      //if no then add as a no to the list ()and ask the next question
           
                      //$question_tags=sprintf("%s###,",$question_tags,$question_id);
                      //put no's in bracket
                      $question_tags = sprintf("%sa%sa,", $question_tags, $question_id);
//echo "<font color=\"#FFFFFF\">last question : $question_id ::Question tags ($question_tags) : ($problem_tags) : ($problem_id)</font><br>";
 
                      $temptags = sprintf("%s%s,", $temptags, $question_id);
                      ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, "6",$iev,$versioninfo,$h,0);
                      break;
                      
                  case "Unsure":
                      //if no or dont know then do nothing and ask the next question
           
                      $question_tags = sprintf("%s###,", $question_tags, $question_id);
//echo "<font color=\"#FFFFFF\">last question : $question_id ::Question tags ($question_tags) : ($problem_tags) : ($problem_id)</font><br>";
 
                      //$question_tags=sprintf("%s(%s),",$question_tags,$question_id); //put no's in bracket
                      $temptags = sprintf("%s%s,", $temptags, $question_id);
                      ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, "6",$iev,$versioninfo,$h,0);
                      
                      break;
                      
                  case "Yes":
                      //if yes then add the question_id to the answer_tags global variable so we can compare 
                      //against a list of solutions in the end
         
                      $question_tags = sprintf("%s%s,", $question_tags, $question_id);
//echo "<font color=\"#FFFFFF\">last question : $question_id ::Question tags ($question_tags) : ($problem_tags) : ($problem_id)</font><br>";
 
                      $temptags = sprintf("%s%s,", $question_tags, $question_id);
                      ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, "6",$iev,$versioninfo,$h,0);
                      break;
                      
                  case "Go":

                      $ret=compute_solution($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id,"8",$h,0,NULL,NULL,$iev,$versioninfo);
                      echo"<script javascript>alert('$ret');</script>";
		      output_xml($ret);
		  break;
              }
          
	break;



	case "2":  // use ACI to choose the most intuitive queston path
//$ret=compute_solution($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id,"8",$h,0,NULL,NULL,$iev,$versioninfo);

//echo "$ret <br>";
   switch ($submit) {

                  case "No":
                      //if no then add as a no to the list ()and ask the next question
           	      // go get all the solutions that have this question answered the same if none then just continue
	              // and then build a new question_tags list based on all the new questions from the solutions found
		      //less the ones we have already asked and add it to the list of questions answered so far        

//go to the solutions db and get a list of all solutions whos tags contain this tag
//$question_tags=$problem_tags;

                      $question_tags = sprintf("%sa%sa,", $question_tags, $question_id);
$p=aci($dbhost, $dbuser, $dbpass, $dbname, $question_id,$problem_tags,$question_tags,$problem_id);
$original_problem_tags=$p[2];
$problem_tags=$p[1];
$h=$p[0];
//echo "<font color=\"#FFFFFF\">last question : $question_id ::Question tags ($question_tags) : ($problem_tags) : ($problem_id)</font><br>";
                      $temptags = sprintf("%s%s,", $temptags, $question_id);
                      ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, "6",$iev,$versioninfo,$h,1);
                      break;
                      
                  case "Unsure":
//$question_tags=$problem_tags;
                      //if no or dont know then do nothing and ask the next question
    
                      $question_tags = sprintf("%s###,", $question_tags, $question_id);
$p=aci($dbhost, $dbuser, $dbpass, $dbname, $question_id,$problem_tags,$question_tags,$problem_id);
$original_problem_tags=$p[2];
$problem_tags=$p[1];
$h=$p[0];
//echo "<font color=\"#FFFFFF\">last question : $question_id ::Question tags ($question_tags) : ($problem_tags) : ($problem_id)</font><br>";

                      //$question_tags=sprintf("%s(%s),",$question_tags,$question_id); //put no's in bracket
                      $temptags = sprintf("%s%s,", $temptags, $question_id);
	
                      ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, "6",$iev,$versioninfo,$h,1);
                      
                      break;
                      
                  case "Yes":
//$question_tags=$problem_tags;
                      //if yes then we need to apply some forward consideration
     	     	      // go get all the solutions that have this question answered the same if none then just continue
	              // and then build a new question_tags list based on all the new questions from the solutions found
		      //less the ones we have already asked and add it to the list of questions answered so far                          
    
                      $question_tags = sprintf("%s%s,", $question_tags, $question_id);
$p=aci($dbhost, $dbuser, $dbpass, $dbname, $question_id,$problem_tags,$question_tags,$problem_id);
$original_problem_tags=$p[2];
$problem_tags=$p[1];
$h=$p[0];
//echo "<font color=\"#FFFFFF\">last question : $question_id ::Question tags ($question_tags) : ($problem_tags) : ($problem_id)</font><br>";
                      $temptags = sprintf("%s%s,", $question_tags, $question_id);
                      ask_question($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id, "6",$iev,$versioninfo,$h,1);
                      break;
                      
                  case "Go":

                     $ret= compute_solution($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id,"8",$h,0,NULL,NULL,$iev,$versioninfo);
echo"<script javascript>alert('$ret');</script>";       
output_xml($ret);               
break;
              }
	
	break;




}

	

}




          
          break;
          
      case 7:
	  //unused case, I dont like 7's
          break;
          
      case 8:
	  include("$skinpath/welcome.html");
          //the problem was not solved so we need to ask to be taught anoter question
          $problem = $problem_description;
          $question_tags_old = $question_tags;
          input_question2($adminpassword,$skinpath,$problem_id, $question_tags, $problem, $question_tags_old, "$skinpath/input_question2.html", "9",$iev,$versioninfo);
          break;        
                    
      case 9:
	  include("$skinpath/welcome.html");
          //the new question has been asked, now add it to the questions database but only build a list of answered or newly asked questions
          $problemx = urldecode($problem);
  	  $question=check_auth($skinpath,$adminpassword,$question,$versioninfo,$iev);      
          save_question2($adminpassword,$skinpath,$question, $dbhost, $dbuser, $dbpass, $dbname, $problem_id, $updateonly, $question_tags, $question_tags_old,$iev,$versioninfo);
          //ask for the solution to this problem
          input_solution($adminpassword,$skinpath,$problem_id, $question_tags, $question_tags_old, "$skinpath/input_solution.html", "4",$iev,$versioninfo,$problemx);
          break;
  }
  
  echo "</body></html>";
?>
