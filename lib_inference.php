<?php
    
  $iev = "V 1.00.13"; //inference engine version number

  
  function compute_solution($adminpassword,$skinpath,$dbhost, $dbuser, $dbpass, $dbname, $question_tags, $problem_tags, $problem_description, $problem_id,$m,$h,$m2,$term_list,$yesno,$iev,$versioninfo)
  {
	$test=0; //setting to 1 will output verbose logging to the screen
        global $question_tags;
        $f = 0;//place holder to count found solutions count    
	if($h=="1")
{
	include("$skinpath/warning_disclaimer.html");
}
     	 // Connect to MySQL Database
 	mysql_connect($dbhost, $dbuser, $dbpass);      
      	mysql_select_db($dbname) or die("Unable to select database");


switch($h)
{
case "0":  //normal fwd chaining question session
case "2";  //aci mode
case "3";

    //get rid of all the unsure answers
      $answers_list = str_replace("###,", "", $question_tags);
      substr_replace($answers_list, "", -1);
      //make a backup for later use
      $answers_list_original = $answers_list;
      
//***************  PASS 1 EXACT MATCHES *******************//

      // first look for exact matches in the solutions tags
      $sql = "SELECT * FROM `solutions` WHERE `solution_tags` = '$answers_list'";      
      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());      
      while ($row = mysql_fetch_assoc($result)) {
          $solution_text = $row['solution_text'];
          $solution_password = $row['solution_password'];
          $f++;
 $solution_text = "I am 100% certain that : $solution_text is the right answer";
      }

//*************************   PASS 2 **************************//     
 
      if ($f == 0)//if no exact match found
	 {
          //if no exact matches are found then check solution with most matching answers
	  //and the least incorrect answers as a %of number of questions
          if($test==1)echo "Original answers $answers_list_original<br>";	
          
          
          $answers = split(",", $answers_list_original);//make an array of answer_id's
	  $num_answers_given=count($answers)-1;//count how many questions are asked to solve this problem

          for ($a = 0; $a < count($answers) - 1; $a++) { //go thru and get a list of all solutions that contain asnswer[$a]
              
              $sql = "SELECT * FROM `solutions`WHERE `solution_tags` LIKE '%$answers[$a]%' ORDER BY `solution_id`";
              $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
              
              while ($row = mysql_fetch_assoc($result)) {
                 
                  $solution_id = sprintf("%d", $row['solution_id']); //get rid of leading zeros                  
                  $solution_text = $row['solution_text'];
                  $solution_tags = $row['solution_tags'];
                  //we must make sure its not found a yes instead of a no answer
                  //if the answers[s] has an 'a' in it then check the tag has an a init hence a NO answer 
                  if (stristr($answers[$a], "a") === false) { //the user said YES tothis question                     
                      $tmp = sprintf("a%sa", $answers[$a]);     //its a yes so turn into a NO and see if thats there                  
                      if (stristr($solution_tags, $tmp) === false) {//if the solution requires a YES add it to list of matches
                          
                         if($test==1) echo "$answers[$a] Possibly: $solution_text :: $solution_tags<br>";
                          $matches = sprintf("%s%s,", $matches, $solution_id); //if user said yes and YES is required then save the solution_id
                          $solution_password = $row['solution_password'];
                          $f++;
                      }
                  } else {//Otherwise if user said no see if a no is required as an answer to this solution
                      if($test==1)echo "$answers[$a] Possibly: $solution_text :: $solution_tags<br>";
                      $matches = sprintf("%s%s,", $matches, $solution_id);
                      $solution_password = $row['solution_password'];
                      $f++;
                  }
              }
          }
                    
          
          //now we have a list of solutions and the number of matching answers that match sloution
          //if we count the number of matches and then check against every solution_tags count we can get a % of correct anwers
          
          $match_list = split(",", $matches);
          for ($a = 0; $a < count($match_list) - 1; $a++) {
              $num_matches = substr_count($matches, $match_list[$a]);
		$num_incorrect=$num_answers_given-$num_matches;
              if($test==1)echo "Solution $match_list[$a] = $num_matches matches $num_incorrect incorrect";
              
              //pad it out with zeros
              $id = sprintf("%-010d", $match_list[$a]);
              
              $sql = "SELECT * FROM `solutions`WHERE `solution_id` = '$id' LIMIT 1";
              
              $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
              
              while ($row = mysql_fetch_assoc($result)) {
                  $solution_id = sprintf("%d", $row['solution_id']);
                  
                  $solution_tags = $row['solution_tags'];
                  $solution_text = $row['solution_text'];
                  $tag_temp = split(",", $solution_tags);
                  $num_tags = count($tag_temp) - 1;
                  if ($num_tags <= 0) {
                      $num_tags = 0;
                  }
                  $percentage_correct = ($num_matches / $num_tags) * 100;
		  $num_wrong=$num_answers_given-$num_matches;
		  $percentage_incorrect= ($num_wrong/$num_matches) * 100;
		  $total_score=$percentage_wrong+$percentage_correct;
                 if($test==1) echo " $solution_text : $percentage_correct% probability of being the correct answer : $num_matches $total_matches<br>";
       
if ($conclusion_percentage <= $total_score & $num_matches >= $total_matches) {
                      //save the most likely answer
                      //$conclusion_percentage = $percentage_correct;
			$conclusion_percentage = $total_score;
			$total_matches=$num_matches;
                      $actual_solution = $solution_text;



                  } else {
                      //save the next most highest possibility
                    //  if ($conclusion_percentage == $percentage_correct & $actual_solution != $solution_text) {
		 if ($conclusion_percentage == $total_score & $actual_solution != $solution_text) {
                          //save the most likely answer
                          //$conclusion_percentage=$percentage_correct;
                          $possible_solution = $solution_text;
                          $possible = "There are other possibilities I am aware of and if I had to take a guess i would say <i>'$possible_solution'</i> could also be the right answer but I would need more information to make a descision.<br>";
                      }
                  }
              }
          }
          
          $conclusion_percentage = sprintf("%2.2f", $conclusion_percentage);
          //echo "Original answers $answers_list_original<br>";
          $solution_text = "I am $conclusion_percentage% certain that : $actual_solution";
	  $possible_text=$possible;
      }
      
//****************  Display the answers or a message saying no answer found *******************//
      if ($f != 0) {
	$solution_text=stripslashes($solution_text);
	$possible_text=stripslashes($possible_text);
	include("$skinpath/output_solution.html");
return($solution_text);
      }
      
      else {
          $question_tags = $answers_list;
          $problemx = urlencode($problem_description);
include("$skinpath/no_conclusion.html");
return(FALSE);
      }
      
      mysql_close();

break;







case "1":  //heuristic

//heuristic searching attempts to understand the question even though Earnest does not have the question in his knowledgebase
//by looking at the question he looks for solutions NOT problems that match the question closely
//he then looks for questions that contain reference to key words in the question and looks for likely answers
//contained by comparing questions with the users question
//we use term list to identify the solution that matches our question closest

$t=count($term_list);
if($test==1)print_r($term_list);
//find all the solutions that contain the "term" in the soultuin text and create a lit of
//all the rule questions that if true would lead us to that answer
		for($n=0;$n<count($term_list);$n++)
		//for($n=0;$n<=0;$n++)
		{
		//is the subject plural ie (dogs)  if yes then save a flag to say so
		$Tempp=$term_list[$n];
		$plural = $term_list[$n][strlen($term_list[$n])-1];//echo "plural: $plural<br>";
		if($plural=="s"){$term_list[$n]=substr($term_list[$n],0,-1); $original_plural=$Tempp;}
		 //if plural then discard the s and flag for later

		$sql = "SELECT * FROM `solutions`WHERE `solution_text` LIKE '% $term_list[$n]%' OR  `solution_text` LIKE '%$term_list[$n] %' ORDER BY `solution_id`";
		if($test==1)echo "<br>$sql<br>"; 
		   
		      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
		      
		      while ($row = mysql_fetch_assoc($result)) {
			  $solution_text = $row['solution_text'];
			  $solution_password = $row['solution_password'];
			  $solution_tags_to_search = sprintf("%s%s",$solution_tags_to_search,$row['solution_tags']);
			  //echo "<br>Solution text: $solution_text back tracing all rules...<br>";        
			  if(strlen(trim($term_list[$n]))!=0){

				if($test==1)echo "<br>Solution text: $solution_text back tracing all rules for term :$term_list[$n]...<br>";

				$termx=sprintf("%ss",$term_list[$n]); //echo "termx:$termx<br>";
				} //try to save the subject ie "Dog"
			  $f++;


		      }

		}
//now we have a list of all the questions that are related to the subject of the question so now do the same thing
//but now look for the term_list in the question_text

$tag_list=split(",",$solution_tags_to_search);
//array_pop($tag_list);
$numtags=count($tag_list)-1;

if($test==1)echo"<br>Tag list :";if($test==1)print_r($tag_list);if($test==1)echo"<br>";

$f=0;
for($a=0;$a<count($term_list);$a++)
	{


		for($n=0;$n<count($tag_list);$n++)
		{
		if($test==1)echo "<br>******<br>Looking in tag $tag_list[$n]<br>";
		$sql = "SELECT * FROM `questions` WHERE `question_id` LIKE '%$tag_list[$n]'   ORDER BY `question_id` ";
		   if($test==1)echo "$sql <br>";   
		      $result = mysql_query($sql) or die("<b>MySQL Error:</b> " . mysql_error());
		      //if($test==1) echo count($term_list);
		      while ($row = mysql_fetch_assoc($result)) {
			  $question_text = $row['question_text'];
		
				$question_text=stripslashes($question_text);
				$question_text=strip_tags($question_text);
				$question_text=preg_replace('/\?/', '', $question_text, 1);
				$question_text=preg_replace('/\'/', '', $question_text, 1);
				$matchscore=0;
				$bestmatch=0;

				for($l=0;$l<count($term_list);$l++)//new
				{
				if($test==1)echo "checking against term $term_list[$l] in $question_text n=$n : $numtags<br>";
					if(preg_match("/\b$term_list[$l]/i", $question_text) & trim($term_list[$l])!="" ) //fixed
						{				
						$matchedtag=$tag_list[$n]; //new
						$matchscore++;		

						if($matchscore > $savedscore)//store the latest best match
						{
							$savedscore=$matchscore;
							$savedtag=$matchedtag;
							$bestmatch=1;
							if($test==1){echo "Savedscore=$savedscore :: $savedtag<br>";}
						}
				
						//do both terms appear in sequence
						$sequence=implode(" ",$term_list);
						if($test==1)echo "$original_plural,$term_list[$l]<br>";
						$sequence=str_replace($term_list[$l],$original_plural,$sequence);

						if($test==1)echo "Sequence $sequence<br>";
						if(@stristr($question_text,$sequence)!=FALSE & trim($sequence)!="" )
						{
							if($test==1)echo "Sequence match<br>";
							$savedscore=$matchscore;
							$savedtag=$matchedtag;
							$termx="";
							$bestmatch=1;
							if($test==1){echo "Savedscore=$savedscore :: $savedtag<br>";}
						}


						if($bestmatch==1) 
						{
						$bestmatch=0;
				
						if($test==1)echo "Match score $matchscore : $matchedtag<br>";
		
				
						      //if this term is found then save the question as a possible
						      //then look for the next term if anothe r question also has hat the stor that question instead
						      if($test==1)echo "Question -> $question_text contains term : $term_list[$l]<br>";
						      $ignore = "is,mean,who,it,if,when,for,and,how,many,what,do,you,can,should,go,the,does,where,of";
						      $profanity = "fuck,fuckoff,fuk,pissoff,wank,shit,cunt,prick,dick,twat,piss,twat,dildo,shag,peado,clit";
						      


						      $ignore_list = split(",", $ignore);
						      for ($r = 0; $r < count($ignore_list); $r++) 
						      {
							  $question_text = str_replace_word($ignore_list[$r], "", $question_text);
						      }
							$question_text=stripslashes($question_text);
							$question_text=strip_tags($question_text);
							$question_text=preg_replace('/\?/', '.', $question_text, 1);
							$question_text=preg_replace('/\'/', '', $question_text, 1);
							$question_text = str_replace_word($termx, "", $question_text);

							//need to modify this function block to see if the answer is very true or a recomended answer
							//need to check if all the terms exist in the answer, if not the modify the output from YEs to Actually I..

							$output=sprintf("<b>Earnest:> </b>%s I believe that %s %s<br>",$yesno,$termx,$question_text);
							if($test==1)echo "output $output<br>";
							//if all the terms exist in the answer then leave as is, otherwise modify $yesno to "Actually, "
							$found=1;
							if($test==1)echo "yesno=$yesno.<br>";
							for($aa=0;$aa<count($term_list);$aa++){

								if(@stristr($output,trim($term_list[$aa]))===FALSE)
								{
									$found=0;
									//$yesno="I don't think so, actually ";
								}


							}
							$finalanswer=sprintf("I believe that %s %s<br>",$termx,$question_text);
							if($test==1)echo "<b>Earnest:> </b>I believe that $termx $question_text<br>";
							$z++;

							if($n==$numtags & trim($finalanswer) !="")
							{
								//echo "<br><br><b>Earnest:> </b>$yesno I believe that $termx $question_text<br>";
								//echo "<br><br><b>$finalanswer<br>";
								//echo "<br>END NNNNNNNNNNNNNNNNNNNNN";
								///exit;
							}
					
							//need to go through again and see if any questions have more matches
						}//end best match
							if($test==1)echo "End best match<br>";
						}
		else
		{
		if($test==1)echo "no match<br>";

		}


		if($test==1)echo "End preg_match<br>";
				}//ned l for inner loopecho "End best match<br>";
		if($test==1)echo "End l loop<br>";
							if($n==$numtags & trim($finalanswer) !="")
							{				
								$amatch=1;
								//need to sanitize the answer and make sure it contains all the terms we set out with otherwise its a flop
								for($u=0;$u<count($term_list);$u++)
								{
									if(!preg_match("/\b$term_list[$u]/i", $finalanswer))
									{
										$amatch=0;
									}
								}


								if($amatch!=0)
								{					
									include("$skinpath/output_heuristic_solution.html");
									exit;
								}
								else
								{
									$z=0;
								}
							}//end if
			}//end while
		

		      }//end for


}//end for
if($z==0)
{
	$finalanswer="From what you are telling me I cannot find a solution. Try asking me the question a little more simply.<br>";
	include("$skinpath/output_heuristic_solution.html");        
}

exit(); //dont go further

break;







}

  
  }
?>
